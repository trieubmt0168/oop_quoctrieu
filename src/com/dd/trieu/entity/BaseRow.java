package com.dd.trieu.entity;

public abstract class BaseRow {
    protected int id;
    protected String name;

    public BaseRow() {

    }

    public BaseRow(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }


    public void setId() {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName() {
        this.name = name;
    }

    public abstract String toString();

}
