package com.dd.trieu.entity;

public class Category extends BaseRow {

    public Category(int id, String name) {
        super(id, name);
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + this.id +
                ", name='" +this.name  + '\'' +
                '}';
    }
}
