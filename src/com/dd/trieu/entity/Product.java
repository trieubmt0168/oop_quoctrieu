package com.dd.trieu.entity;

public class Product extends BaseRow {


    public Product(int id, String name) {
        super(id, name);
    }

    @Override
    public String toString() {
        return "ProductEntity{" +
                "id=" + this.id +
                ", name='" + this.name + '\'' +

                '}';
    }


}