package com.dd.trieu.dao;

import com.dd.trieu.entity.BaseRow;
import com.dd.trieu.entity.Category;
import com.dd.trieu.entity.Product;

import java.util.ArrayList;
import java.util.List;

public class Database {
    private ArrayList<BaseRow> productTable;
    private ArrayList<BaseRow> categoryTable;
    private ArrayList<BaseRow> accessoryTable;
    private Database instants;


    public Database() {
        this.productTable = new ArrayList<>();
        this.categoryTable = new ArrayList<>();
        this.accessoryTable = new ArrayList<>();
    }

    public ArrayList<BaseRow> getProductTable() {
        return productTable;
    }

    public void setProductTable(ArrayList<BaseRow> productTable) {
        this.productTable = productTable;
    }

    public ArrayList<BaseRow> getCategoryTable() {
        return categoryTable;
    }

    public void setCategoryTable(ArrayList<BaseRow> categoryTable) {
        categoryTable = categoryTable;
    }

    public ArrayList<BaseRow> getAccessoryTable() {
        return accessoryTable;
    }

    public void setAccessoryTable(ArrayList<BaseRow> accessoryTable) {
        this.accessoryTable = accessoryTable;
    }

    public Database getInstants() {
        return instants;
    }

    public void setInstants(Database instants) {
        this.instants = instants;
    }

    @Override
    public String toString() {
        return "Database{" +
                "product=" + productTable +
                ", CategoryTable=" + categoryTable +
                ", accessoryTable=" + accessoryTable +
                ", instants=" + instants +
                '}';
    }


    public int insertTable(String name, BaseRow row) {
        int i = 0;
        if (name.equals("product")) {
            this.productTable.add(row);
            i = 1;
        }
        if (name.equals("category")) {
            this.categoryTable.add(row);
            i = 1;
        }
        if (name.equals("accessory")) {
            this.accessoryTable.add(row);
            i = 1;
        }
        return i;
    }

    public ArrayList<BaseRow> selectTable(String name) {
        if (name.equals("product")) {
            return this.productTable;
        }
        if (name.equals("category")) {
            return this.categoryTable;
        }
        if (name.equals("accessory")) {
            return this.accessoryTable;
        }
        return null;
    }

    public Integer updateTable(String name, BaseRow oldRow, BaseRow newRow) {
        int i = 0;

        ArrayList<BaseRow> objCategory = selectTable("category");
        ArrayList<BaseRow> objProduct = selectTable("product");
        ArrayList<BaseRow> objAccessory = selectTable("accessory");

        Integer oldcategory = objCategory.indexOf(oldRow);
        Integer oldproduct = objProduct.indexOf(oldRow);
        Integer oldaccessory = objAccessory.indexOf(oldRow);
        if (name.equals("product")) {
            this.productTable.set(oldproduct, newRow);
            i = 1;
        }
        if (name.equals("category")) {
            this.categoryTable.set(oldcategory, newRow);
            i = 1;
        }
        if (name.equals("accessory")) {
            this.accessoryTable.set(oldaccessory, newRow);
            i = 1;
        }
        return i;
    }

    public Boolean deleteTable(String name, BaseRow row) {

        if (name.equals("product")) {
            this.productTable.remove(row);

        }
        if (name.equals("category")) {
            this.categoryTable.remove(row);

        }
        if (name.equals("accessory")) {
            this.accessoryTable.remove(row);
        }
        return false;
    }

    public void truncateTable(String name) {
        if (name.equals("product")) {
            this.productTable.clear();
            System.out.println("TRuncateProduct");
        }
        if (name.equals("categoryTable1")) {
            this.categoryTable.clear();
            System.out.println("TruncateCategory");
        }
        if (name.equals("accessoryTable1")) {
            this.categoryTable.clear();
            System.out.println("TruncateAccesory");
        }
    }

    public Integer updateTable1(String name, int id, BaseRow newRow) {
//        int check = 1;
////
////        if (name.equals("product")) {
////            ArrayList<BaseRow> objProduct = selectTable("product");
////            System.out.println(objProduct);
////            BaseRow product1 = new Product( );
////
////            for (int i = 0; i < objProduct.size(); i++) {
////                BaseRow product2 = objProduct.get(i);
////                if (id == product2.getId()) {
////                    product1 = product2;
////                }
////                Integer index = objProduct.indexOf(product1);
////
////                this.productTable.set(index, newRow);
////                check = 0;
////            }
////        }
        int check = 0;
        if (name.equals("product")) {
            // Dng stream
            ArrayList<BaseRow> listProducts = selectTable("product");
            BaseRow product = listProducts.stream().filter(products -> id == products.getId()).findAny().orElse(null);
            Integer index = listProducts.indexOf(product);
            this.productTable.set(index, newRow);
            System.out.println("" + index);
            check = 1;
        }
        return check;

    }
}
