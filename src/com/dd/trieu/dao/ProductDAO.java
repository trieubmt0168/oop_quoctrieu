package com.dd.trieu.dao;

import com.dd.trieu.entity.Accessory;
import com.dd.trieu.entity.BaseRow;
import com.dd.trieu.entity.Product;

import java.util.ArrayList;

public class ProductDAO extends BaseDao {

    public ProductDAO() {
        this.tableName = "product";
    }


    public Integer insert(Product row1) {
        return super.insert(row1);
    }

    public int update(Product oldBaseRow, Product newBaseRow) {

        return super.update(oldBaseRow,newBaseRow);
    }

    public boolean delete(Product baseRow) {

        return super.delete(baseRow);
    }

    public ArrayList<BaseRow> findAll() {

        return super.findAll();
    }

    public Product findById(int id) {
        return (Product) super.findById(id);
    }


    public Product findByName(String name) {

        return (Product) super.findByName(name);
    }

}





