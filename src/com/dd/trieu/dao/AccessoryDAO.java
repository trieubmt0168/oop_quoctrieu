package com.dd.trieu.dao;

import com.dd.trieu.entity.Accessory;

import com.dd.trieu.entity.BaseRow;


import java.util.ArrayList;

public class AccessoryDAO extends BaseDao {

    public AccessoryDAO() {
        super();
        this.tableName = "accessory";
    }

    public Integer insert(Accessory row1) {
        return super.insert(row1);
    }

    public int update(Accessory oldBaseRow, Accessory newBaseRow) {

        return super.update(oldBaseRow,newBaseRow);
    }

    public boolean delete(Accessory baseRow) {

        return super.delete(baseRow);
    }

    public ArrayList<BaseRow> findAll() {

        return super.findAll();
    }

    public Accessory findById(int id) {

        return (Accessory) super.findById(id);
    }


    public Accessory findByName(String name) {

        return (Accessory) super.findByName(name);
    }


}