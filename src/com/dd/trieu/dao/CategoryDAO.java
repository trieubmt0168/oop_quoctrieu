package com.dd.trieu.dao;


import com.dd.trieu.entity.BaseRow;
import com.dd.trieu.entity.Category;

import java.util.ArrayList;

public class CategoryDAO extends BaseDao {

    public CategoryDAO() {
        this.tableName = "category";
    }
    public Integer insert(Category row1) {
        return super.insert(row1);
    }

    public int update(Category oldBaseRow, Category newBaseRow) {

        return super.update(oldBaseRow,newBaseRow);
    }

    public boolean delete(Category baseRow) {

        return super.delete(baseRow);
    }

    public ArrayList<BaseRow> findAll() {

        return super.findAll();
    }

    public Category findById(int id) {
        return (Category) super.findById(id);
    }


    public Category findByName(String name) {

        return (Category) super.findByName(name);
    }

}


