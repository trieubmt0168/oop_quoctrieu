package com.dd.trieu.dao;

import com.dd.trieu.entity.BaseRow;

import java.util.ArrayList;

public abstract class BaseDao {

    private static Database database = new Database();

    protected String tableName ;


    protected Integer insert(BaseRow row1) {
        int i = database.insertTable(tableName, row1);
        return i;
    }


    protected int update(BaseRow oldBaseRow, BaseRow newBaseRow) {
        int i = database.updateTable(tableName, oldBaseRow, newBaseRow);
        return i;
    }

    protected boolean delete(BaseRow baseRow) {
        boolean check = database.deleteTable(tableName, baseRow);
        return check;
    }

    protected ArrayList<BaseRow> findAll() {
        ArrayList<BaseRow> category = database.selectTable(tableName);
        return category;
    }

    protected BaseRow findById(int id) {
        ArrayList<BaseRow> categorys = database.selectTable(tableName);
        BaseRow category = categorys.stream().filter(category1 -> id == category1.getId()).findAny().orElse(null);
        return category;
    }


    protected BaseRow findByName(String name) {
        ArrayList<BaseRow> categorys = database.selectTable(tableName);
        BaseRow category = categorys.stream().filter(category1 -> name == category1.getName()).findAny().orElse(null);
        return category;
    }


}
