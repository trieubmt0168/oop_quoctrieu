package com.dd.trieu.demo;

import com.dd.trieu.dao.Database;
import com.dd.trieu.entity.Accessory;
import com.dd.trieu.entity.BaseRow;
import com.dd.trieu.entity.Category;
import com.dd.trieu.entity.Product;

import java.util.ArrayList;
import java.util.List;

public class DatabaseDemo {
   private static Database database = new Database();

    private static void insertTableTest() {
        DatabaseDemo databaseDemo = new DatabaseDemo();
        Category category = new Category(1, "category");
        Accessory accessory = new Accessory(2, "accessory");
        try {
            Integer i1 = database.insertTable("category", category);
            Integer i2 = database.insertTable("accessory", accessory);
            System.out.println(i1);
            System.out.println(i2);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    private static void selectTableTest() {
        Category category = new Category(1, "Long");
        Integer i = database.insertTable("category", category);
        List<BaseRow> categorys = database.selectTable("category");
        System.out.println(categorys);
        System.out.println("**********************");
        Accessory accessory = new Accessory(2, "Thái acc");
        Integer i1 = database.insertTable("accessory", accessory);
        List<BaseRow> accessorys = database.selectTable("accessory");
        System.out.println(accessorys);
    }

    private static void updateTableTest() {
        System.out.println("UpdateTbale");
        ArrayList<BaseRow> objectArrayList = database.selectTable("category");
        BaseRow categoryOld = objectArrayList.get(0);
        BaseRow categoryNew = new Category(1, "Trieu");
        database.updateTable("categoryTable", categoryOld, categoryNew);
        System.out.println("categoryOld" + categoryOld + "----" + "categoryNew" + categoryNew);
    }

    private static Boolean deleteTableTest() {
        System.out.println("delete table");
        List<BaseRow> categorys = database.selectTable("category");
        BaseRow category = categorys.get(0);
        boolean delete = database.deleteTable("category", category);
        System.out.println("Delete :" + category + "status : " + delete);
        return delete;
    }

    private static void truncateTableTest() {
        database.truncateTable("category");
    }

    private static void initDatabase() {
        Accessory accessory = new Accessory(1, "A1");
        Accessory accessory1 = new Accessory(2, "A2");
        Accessory accessory2 = new Accessory(3, "A3");
        Accessory accessory3 = new Accessory(4, "A4");
        Accessory accessory4 = new Accessory(5, "A5");
        Accessory accessory5 = new Accessory(6, "A6");
        Accessory accessory6 = new Accessory(7, "A7");
        Accessory accessory7 = new Accessory(8, "A8");
        Accessory accessory8 = new Accessory(9, "A9");
        Accessory accessory9 = new Accessory(10, "A10");
        System.out.println("***********");
        Product product = new Product(1, "D1");
        Product product1 = new Product(2, "D2");
        Product product2 = new Product(3, "D3");
        Product product3 = new Product(4, "D4");
        Product product4 = new Product(5, "D5");
        Product product5 = new Product(6, "D6");
        Product product6 = new Product(7, "D7");
        Product product7 = new Product(8, "D8");
        Product product8 = new Product(9, "D9");
        Product product9 = new Product(10, "D10");
        System.out.println("************");
        Category category = new Category(1, "c1");
        Category category1 = new Category(2, "c2");
        Category category2 = new Category(3, "c3");
        Category category3 = new Category(4, "c4");
        Category category4 = new Category(5, "c5");
        Category category5 = new Category(6, "c6");
        Category category6 = new Category(7, "c7");
        Category category7 = new Category(8, "c8");
        Category category8 = new Category(9, "c9");
        Category category9 = new Category(10, "c10");

        database.insertTable("accessory", accessory);
        database.insertTable("accessory", accessory1);
        database.insertTable("accessory", accessory2);
        database.insertTable("accessory", accessory3);
        database.insertTable("accessory", accessory4);
        database.insertTable("accessory", accessory5);
        database.insertTable("accessory", accessory6);
        database.insertTable("accessory", accessory7);
        database.insertTable("accessory", accessory8);
        database.insertTable("accessory", accessory9);
        System.out.println("*******");
        database.insertTable("product", product);
        database.insertTable("product", product1);
        database.insertTable("product", product2);
        database.insertTable("product", product3);
        database.insertTable("product", product4);
        database.insertTable("product", product5);
        database.insertTable("product", product6);
        database.insertTable("product", product7);
        database.insertTable("product", product8);
        database.insertTable("product", product9);
        System.out.println("**********");
        database.insertTable("category", category);
        database.insertTable("category", category1);
        database.insertTable("category", category2);
        database.insertTable("category", category3);
        database.insertTable("category", category4);
        database.insertTable("category", category5);
        database.insertTable("category", category6);
        database.insertTable("category", category7);
        database.insertTable("category", category8);
        database.insertTable("category", category9);


    }

    private static void printTableTest() {
        System.out.println("Print Table : ");
        ArrayList<BaseRow> products = database.selectTable("product");
        System.out.println(products);
        ArrayList<BaseRow> categorys = database.selectTable("category");
        System.out.println(categorys);
        ArrayList<BaseRow> accessorys = database.selectTable("accessory");
        System.out.println(accessorys);
    }

    private static void updateTableTest1() {
        System.out.println("UpdateTbale");
        BaseRow productNew = new Product(1,"tÁO");
        database.updateTable1("product", 1, productNew);
        System.out.println(database.selectTable("product"));
    }

    public static void main(String[] args) {
        insertTableTest();
        selectTableTest();
        updateTableTest();
        deleteTableTest();
        truncateTableTest();
        initDatabase();
        printTableTest();
        updateTableTest1();

    }
}
