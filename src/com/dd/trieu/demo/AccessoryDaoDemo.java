package com.dd.trieu.demo;

import com.dd.trieu.dao.AccessoryDAO;
import com.dd.trieu.entity.Accessory;
import com.dd.trieu.entity.BaseRow;

import java.util.ArrayList;
import java.util.List;

public class AccessoryDaoDemo {
    private static AccessoryDAO accessoryDAO = new AccessoryDAO();

    private static Integer insertTest() {
        Accessory accessory = new Accessory(1, "A1");
        Accessory accessory1 = new Accessory(2, "A2");
        Accessory accessory2 = new Accessory(3, "A3");
        Accessory accessory3 = new Accessory(4, "A4");
        int check;
        if (accessoryDAO.insert(accessory) == 1 && accessoryDAO.insert(accessory1) == 1 && accessoryDAO.insert(accessory2) == 1
                && accessoryDAO.insert(accessory3) == 1) {
            check = 1;
            System.out.println("" + "" + true);
        } else {
            check = 1;
            System.out.println("" + "" + false);
        }
        return check;

    }

    private static ArrayList<BaseRow> findAllTest() {
        System.out.println("-------Find accessory--------");
        ArrayList<BaseRow> accessory = accessoryDAO.findAll();
        System.out.println(accessory);
        return accessory;

    }

    private static void updateTest() {

        ArrayList<BaseRow> accessoryArrayList = accessoryDAO.findAll();

        BaseRow accessoryOld = accessoryArrayList.get(1);
        Accessory accessoryNew = new Accessory(2, "A22");
        accessoryDAO.update((Accessory) accessoryOld, accessoryNew);
        System.out.println("accessoryOld" + accessoryOld + "----" + "accessoryNew" + accessoryNew);
    }

    private static boolean daleteTest() {
        boolean Kt;
        System.out.println("delete table");
        ArrayList<BaseRow> accessoryDAOAll = accessoryDAO.findAll();
        BaseRow delete = accessoryDAOAll.get(2);
        Kt = accessoryDAO.delete((Accessory) delete);
        System.out.println("Delete :" + delete + "status : " + Kt);
        List<BaseRow> accessories = accessoryDAO.findAll();
        System.out.println("---Dữ liệu còn lại sau khi xóa");
        System.out.println(accessories);
        return Kt;
    }

    private static void findIdTest() {
        System.out.println("tìm theo ID");
        BaseRow accessory = accessoryDAO.findById(2);
        System.out.println(accessory);
    }

    private static void findNameTest() {
        System.out.println("tìm theo tên");
        BaseRow accessory = accessoryDAO.findByName("A1");
        System.out.println(accessory);
    }

    public static void main(String[] args) {
        insertTest();
        findAllTest();
        updateTest();
        daleteTest();
        findIdTest();
        findNameTest();
    }
}


