package com.dd.trieu.demo;

import com.dd.trieu.dao.CategoryDAO;
import com.dd.trieu.entity.BaseRow;
import com.dd.trieu.entity.Category;
import java.util.ArrayList;
import java.util.List;

public class CategoryDaoDemo {

   private static CategoryDAO categoryDAO = new CategoryDAO();

    private static Integer insertTest() {
        Category category = new Category(1, "Cam");
        Category category1 = new Category(2, "Quýt");
        Category category2 = new Category(3, "Bưởi");
        Category category3 = new Category(4, "Na");
        int check;
        if (categoryDAO.insert(category) == 1 && categoryDAO.insert(category1) == 1 && categoryDAO.insert(category2) == 1
                && categoryDAO.insert(category3) == 1) {
            check = 1;
            System.out.println("" + "" + true);
        } else {
            check = 1;
            System.out.println("" + "" + false);
        }
        return check;
    }

    public static ArrayList<BaseRow> findAllTest() {
        System.out.println("-------Find category--------");
        ArrayList<BaseRow> categorys = categoryDAO.findAll();
        System.out.println(categorys);
        return categorys;
    }

    public static void updateTest() {
        ArrayList<BaseRow> objectArrayList = categoryDAO.findAll();
        BaseRow categoryOld = objectArrayList.get(1);
        Category categoryNew = new Category(2, "QQQQ");
        categoryDAO.update((Category) categoryOld, categoryNew);
        System.out.println("categoryOld" + categoryOld + "----" + "categoryNew" + categoryNew);
    }

    public static boolean daleteTest() {
        boolean Kt;
        System.out.println("delete table");
        ArrayList<BaseRow> categorys = categoryDAO.findAll();
        BaseRow delete = categorys.get(2);
        Kt = categoryDAO.delete((Category) delete);
        System.out.println("Delete :" + delete + "status : " + Kt);
        List<BaseRow> categories = categoryDAO.findAll();
        System.out.println(categories);
        return Kt;
    }

    private static void findIdTest() {
        System.out.println("tìm theo ID");
        BaseRow accessory = categoryDAO.findById(2);
        System.out.println(accessory);
    }

    private static void findNameTest() {
        System.out.println("tìm theo tên");
        BaseRow accessory = categoryDAO.findByName("Cam");
        System.out.println(accessory);
    }

    public static void main(String[] args) {
        insertTest();
        findAllTest();
        updateTest();
        daleteTest();
        findIdTest();
        findNameTest();

    }
}
