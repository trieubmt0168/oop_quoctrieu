package com.dd.trieu.demo;

//import com.dd.trieu.dao.DatabaseProduct;

import com.dd.trieu.dao.ProductDAO;
import com.dd.trieu.entity.BaseRow;
import com.dd.trieu.entity.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductDaoDemo {
     private static ProductDAO productDAO = new ProductDAO();

    private static Integer insertTest() {
        Product product = new Product(1, "A1");
        Product product1 = new Product(2, "A2");
        Product product2 = new Product(3, "A3");
        Product product3 = new Product(4, "A4");
        int check;
        if (productDAO.insert(product) == 1 && productDAO.insert(product1) == 1 && productDAO.insert(product2) == 1
                && productDAO.insert(product3) == 1) {
            check = 1;
            System.out.println("" + "" + true);
        } else {
            check = 1;
            System.out.println("" + "" + false);
        }
        return check;

    }


    private static ArrayList<BaseRow> findAllTest() {
        System.out.println("-------Find product--------");
        ArrayList<BaseRow> products = productDAO.findAll();
        System.out.println(products);
        return products;

    }

    private static void updateTest() {

        ArrayList<BaseRow> productArrayList = productDAO.findAll();

        Product productOld = (Product) productArrayList.get(2);
        Product productNew = new Product(3, "T#");
        productDAO.update( productOld, productNew);
        System.out.println("productOld" + productOld + "----" + "productNew" + productNew);
    }

    private static boolean daleteTest() {

        boolean Kt;
        System.out.println("delete table");
        ArrayList<BaseRow> productss = productDAO.findAll();
        BaseRow delete = productss.get(2);
        Kt = productDAO.delete((Product) delete);
        System.out.println("Delete :" + delete + "status : " + Kt);
        List<BaseRow> products = productDAO.findAll();

        System.out.println(products);
        return Kt;
    }

    private static void findIdTest() {
        System.out.println("tìm theo ID");
        BaseRow product = productDAO.findById(2);
        System.out.println(product);
    }

    private static void findNameTest() {
        System.out.println("tìm theo tên");
        BaseRow product = (BaseRow) productDAO.findByName("A2");
        System.out.println(product);
    }

    public static void main(String[] args) {
        insertTest();
        findAllTest();
        updateTest();
        daleteTest();
        findIdTest();
        findNameTest();
    }
}
