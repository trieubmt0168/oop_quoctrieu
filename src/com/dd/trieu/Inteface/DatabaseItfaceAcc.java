package com.dd.trieu.Inteface;

import com.dd.trieu.entity.Accessory;

import java.util.ArrayList;

public interface DatabaseItfaceAcc
{
    public int insertTable(String name, Accessory  accessory);
    public ArrayList<Accessory> selectTable(String name);
    public Integer updateTable(String name, Accessory oldaccessory, Accessory newaccessory);
    public Boolean deleteTable(String name, Accessory accessory);
    public void truncateTable(String name);
    public Integer updateTable1(String name, int id, Accessory accessory);

}
